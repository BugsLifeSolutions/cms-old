/*
 Copyright (C) 2015  PencilBlue, LLC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//dependencies
var path  = require('path');
var async = require('async');
var _     = require('underscore');

module.exports = function categoryObject(pb) {

    //pb dependencies
    var util           = pb.util;
    var config         = pb.config;
    var TopMenu        = pb.TopMenuService;
    var Comments       = pb.CommentService;
    var ArticleService = pb.ArticleService;

    /**
     * Index page of the portfolio plugin
     * @deprecated Since 0.4.1
     * @class Index
     * @constructor
     * @extends BaseController
     */
    function categoryObject(){}
    util.inherits(categoryObject, pb.BaseController);

    categoryObject.prototype.render = function(cb) {
        var self = this;
        var sectionName = this.pathVars.section_name;
        self.ts.registerLocal('section_name', new pb.TemplateValue(sectionName, false));
        var categoryName = this.pathVars.category_name;
        self.ts.registerLocal('category_name', new pb.TemplateValue(categoryName, false));

        // Get the Product Section Display Name
        var opts = {
            where: {name: 'Product Sections'},
            limit: 1
        };
        var daoSectionType = new pb.DAO();
        daoSectionType.q('custom_object_type', opts, function(err, sectionObjectType) {
            if (util.isError(err)) {
                return self.serveError(err);
            }
            else if (!util.isObject(sectionObjectType)) {
                return self.reqHandler.serve404();
            }
            var sectionTypeObjectId = self.stringToHex(sectionObjectType[0]._id.id);

            var daoSection = new pb.DAO();
            daoSection.q('custom_object', {
                where: {type: sectionTypeObjectId, name: sectionName},
                limit: 1
            }, function (err, sectionObject) {
                if (util.isError(err)) {
                    return self.serveError(err);
                }
                if (sectionObject.length == 0) {
                    return self.reqHandler.serve404();
                }
                self.ts.registerLocal('section_display_name', new pb.TemplateValue(sectionObject[0]['Display Name'], false));
            });
        });

        var contentService = new pb.ContentService();
        contentService.getSettings(function(err, contentSettings) {
            self.gatherData(function(err, data) {

                var articleService = new pb.ArticleService();
                articleService.getMetaInfo(data.content[0], function(err, meta) {
                    self.ts.registerLocal('navigation', new pb.TemplateValue(data.nav.navigation, false));
                });
            });
        });

        // Custom Product Category
        var daoCategory = new pb.DAO();
        daoCategory.q('custom_object', {where: {name: categoryName}, limit: 1}, function(err, categoryObject) {
            if (util.isError(err)) {
                return self.serveError(err);
            }
            else if (!util.isObject(categoryObject[0])) {
                return self.reqHandler.serve404();
            }

            //Load the content for the page
            self.ts.registerLocal('page_headline', new pb.TemplateValue(categoryObject[0]['Display Name'], false));
            self.ts.registerLocal('page_content', new pb.TemplateValue(categoryObject[0].Content, false));
            self.ts.registerLocal('category_display_name', new pb.TemplateValue(categoryObject[0]['Display Name'], false));

            var daoMediaObjects = new pb.DAO();
            var mediaObjectResults = [];
            if(categoryObject[0].Media.length != 0) {
                for(var indx = 0; indx < categoryObject[0].Media.length; indx++) {
                    (function(indx_copy) {
                        daoMediaObjects.loadById(categoryObject[0].Media[indx_copy], 'media', {}, function(err, mediaResults) {
                            if (util.isError(mediaResults)) {
                                return self.reqHandler.serveError(err);
                            }
                            mediaObjectResults.push(mediaResults);

                            if(indx_copy == categoryObject[0].Media.length - 1) {
                                var mediaObjects = pb.ClientJs.getAngularObjects({
                                    mediaObjects: mediaObjectResults
                                });
                                self.ts.registerLocal('media_objects', new pb.TemplateValue(mediaObjects, false));
                            }
                        });
                    })(indx);
                }
            }

            var mediaObjects = pb.ClientJs.getAngularObjects({
                mediaObjects: []
            });
            self.ts.registerLocal('media_objects', new pb.TemplateValue(mediaObjects, false));

            var productObjectResults = [];
            _.each(categoryObject[0].Products, function(productId) {
                var daoProducts = new pb.DAO();

                var placeHolderProduct = {};
                productObjectResults.push(placeHolderProduct);
                daoProducts.loadById(productId, 'custom_object', {}, function(err, productObject) {
                    if (util.isError(productObject)) {
                        return self.reqHandler.serveError(err);
                    }

                    var imageUrl = '/public/bl-demo/img/placeholders/placeholder.png';
                    var imageName = 'Placeholder';
                    var count = 0;
                    _.each(productObject.Media, function(mediaId) {
                        count++;
                        var daoProductMedia = new pb.DAO();
                        (function(count_copy) {
                            daoProductMedia.loadById(mediaId, 'media', {}, function(err, mediaObject) {
                                if(util.isError(mediaObject)) {
                                    return self.reqHandler.serveError(err);
                                }
                                if(mediaObject !== null && mediaObject.media_type == 'image') {
                                    imageUrl = mediaObject.location;
                                    imageName = mediaObject.name;
                                }

                                productObject['imageUrl'] = imageUrl;
                                productObject['imageName'] = imageName;

                                if(count_copy == productObject.Media.length) {
                                    _.extendOwn(placeHolderProduct, productObject);

                                    //Register the angular array of product objects
                                    var angularObjects = pb.ClientJs.getAngularObjects({ productObjects: productObjectResults });
                                    self.ts.registerLocal('product_objects', new pb.TemplateValue(angularObjects, false));
                                }
                            });
                        })(count);
                    });

                    productObject['imageUrl'] = imageUrl;
                    productObject['imageName'] = imageName;

                    _.extendOwn(placeHolderProduct, productObject);

                    //Register the angular array of product objects
                    var angularObjects = pb.ClientJs.getAngularObjects({ productObjects: productObjectResults });
                    self.ts.registerLocal('product_objects', new pb.TemplateValue(angularObjects, false));
                });
            });

            //Register the angular array of product objects
            var angularObjects = pb.ClientJs.getAngularObjects({ productObjects: productObjectResults });
            self.ts.registerLocal('product_objects', new pb.TemplateValue(angularObjects, false));

            self.ts.registerLocal('page_name', new pb.TemplateValue(categoryObject[0]['Display Name'] + ' | Demo Website', false)); //TODO: Stop using hard coded values

            self.ts.load('category', function(err, result) {
                if (util.isError(err)) {
                    return cb(err);
                }
                cb({content: result});
            });
        });
    };

    categoryObject.prototype.gatherData = function(cb) {
        var self  = this;
        var tasks = {

            //navigation
            nav: function(callback) {
                self.getNavigation(function(themeSettings, navigation, accountButtons) {
                    callback(null, {themeSettings: themeSettings, navigation: navigation, accountButtons: accountButtons});
                });
            },

            //articles, pages, etc.
            content: function(callback) {
                self.loadContent(callback);
            },

            section: function(callback) {
                if(!self.req.pencilblue_section) {
                    callback(null, {});
                    return;
                }

                var dao = new pb.DAO();
                dao.loadById(self.req.pencilblue_section, {}, 'section', callback);
            }
        };
        async.parallel(tasks, cb);
    };

    categoryObject.prototype.loadContent = function(articleCallback) {

        var section = this.req.pencilblue_section || null;
        var topic   = this.req.pencilblue_topic   || null;
        var article = this.req.pencilblue_article || null;
        var page    = this.req.pencilblue_page    || null;

        //get service context
        var opts = this.getServiceContext();

        var service = new ArticleService();
        if(this.req.pencilblue_preview) {
            if(this.req.pencilblue_preview == page || article) {
                if(page) {
                    service.setContentType('page');
                }
                var where = pb.DAO.getIdWhere(page || article);
                where.draft = {$exists: true};
                where.publish_date = {$exists: true};
                service.find(where, opts, articleCallback);
            }
            else {
                service.find({}, opts, articleCallback);
            }
        }
        else if(section) {
            service.findBySection(section, articleCallback);
        }
        else if(topic) {
            service.findByTopic(topic, articleCallback);
        }
        else if(article) {
            service.findById(article, articleCallback);
        }
        else if(page) {
            service.setContentType('page');
            service.findById(page, articleCallback);
        }
        else{
            service.find({}, opts, articleCallback);
        }
    };

    categoryObject.prototype.getNavigation = function(cb) {
        var options = {
            currUrl: this.req.url,
            session: this.session,
            ls: this.ls,
            activeTheme: this.activeTheme
        };

        var menuService = new pb.TopMenuService();
        menuService.getNavItems(options, function(err, navItems) {
            if (util.isError(err)) {
                pb.log.error('categoryObject[0]: %s', err.stack);
            }
            cb(navItems.themeSettings, navItems.navigation, navItems.accountButtons);
        });
    };

    categoryObject.prototype.stringToHex = function (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;

        for (; i < tmp_len; i += 1) {
            c = tmp.charCodeAt(i);
            str += this.d2h(c);
        }
        return str;
    };
    categoryObject.prototype.d2h = function(number) {
        if (number < 0) {
            number = 0xFFFFFFFF + number + 1;
        }
        return this.pad(number.toString(16), 2);
    };
    categoryObject.prototype.pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    /**
     * Provides the routes that are to be handled by an instance of this prototype.
     * The route provides a definition of path, permissions, authentication, and
     * expected content type.
     * Method is optional
     * Path is required
     * Permissions are optional
     * Access levels are optional
     * Content type is optional
     *
     * @param cb A callback of the form: cb(error, array of objects)
     */
    categoryObject.getRoutes = function(cb) {
        var routes = [
            {
                method: 'get',
                path: '/page/products/:section_name/:category_name',
                auth_required: false,
                content_type: 'text/html'
            }
        ];
        cb(null, routes);
    };

    //exports
    return categoryObject;
};