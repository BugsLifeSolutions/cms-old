/**
 * Created by HadenHiles on 15-10-15.
 */
/*
 Copyright (C) 2015  PencilBlue, LLC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//dependencies
var path  = require('path');
var async = require('async');
var _     = require('underscore');

module.exports = function PriceObjects(pb) {
    //pb dependencies
    var util           = pb.util;

    /**
     * Print page of the product prices in the bl-products plugin
     * @constructor
     * @extends BaseController
     */
    function PriceObjects(){}
    util.inherits(PriceObjects, pb.BaseController);

    PriceObjects.prototype.render = function(cb) {
        var self = this;

        //Load product sections
        var daoSectionTypes = new pb.DAO();
        var sectionTypeOpts = {
            where: { name: "Product Sections" },
            limit: 1
        };
        daoSectionTypes.q('custom_object_type', sectionTypeOpts, function(err, sectionTypeObject) {
            if(util.isError(err)) {
                return self.serveError(err);
            }

            var sectionTypeObjectId = self.stringToHex(sectionTypeObject[0]._id.id);

            var daoSections = new pb.DAO();
            var sectionOpts = {
                where: { type: sectionTypeObjectId },
                order: { 'Display Name': 1 }
            };
            daoSections.q('custom_object', sectionOpts, function(err, sectionObjects) {
                if(util.isError(err)) {
                    return self.reqHandler.serveError(err);
                }

                _.each(sectionObjects, function(sectionObject) {
                    var imageUrl = '/public/bl-demo/img/placeholders/placeholder.png';
                    var imageName = 'Placeholder';
                    _.each(sectionObject.Media, function(mediaId) {
                        var daoSectionMedia = new pb.DAO();
                        daoSectionMedia.loadById(mediaId, 'media', {}, function(err, mediaObject) {
                            if(util.isError(err)) {
                                return self.reqHandler.serveError(err);
                            }
                            if(mediaObject !== null && mediaObject.media_type == 'image') {
                                imageUrl = mediaObject.location;
                                imageName = mediaObject.name;
                            }

                            sectionObject['imageUrl'] = imageUrl;
                            sectionObject['imageName'] = imageName;

                            //Register the angular array of section objects
                            var angularObjects = pb.ClientJs.getAngularObjects({ sectionObjects: sectionObjects });
                            self.ts.registerLocal('section_objects', new pb.TemplateValue(angularObjects, false));
                        });
                    });
                    sectionObject['imageUrl'] = imageUrl;
                    sectionObject['imageName'] = imageName;

                    var categoryObjectResults = [];
                    _.each(sectionObject.Categories, function(categoryId) {
                        var daoCategory = new pb.DAO();
                        daoCategory.loadById(categoryId, 'custom_object', {}, function(err, categoryObject) {
                            if(util.isError(err)) {
                                return self.reqHandler.serveError(err);
                            }
                            if(categoryObject != null) {
                                //Update each category object to include their image url's and names
                                var imageUrl = '/public/bl-demo/img/placeholders/placeholder.png';
                                var imageName = 'Placeholder';
                                if(categoryObject.Media.length > 0) {
                                    _.each(categoryObject.Media, function(mediaId) {
                                        var daoCategoryMedia = new pb.DAO();
                                        daoCategoryMedia.loadById(mediaId, 'media', {}, function(err, mediaObject) {
                                            if(util.isError(err)) {
                                                return self.reqHandler.serveError(err);
                                            }
                                            if(mediaObject !== null && mediaObject.media_type == 'image') {
                                                imageUrl = mediaObject.location;
                                                imageName = mediaObject.name;
                                            }

                                            categoryObject['imageUrl'] = imageUrl;
                                            categoryObject['imageName'] = imageName;

                                            //Add the categories to the current section Object
                                            sectionObject['categoryObjects'] = categoryObjectResults;
                                        });
                                    });
                                    categoryObject['imageUrl'] = imageUrl;
                                    categoryObject['imageName'] = imageName;
                                } else {
                                    categoryObject['imageUrl'] = imageUrl;
                                    categoryObject['imageName'] = imageName;

                                    //Add the categories to the current section Object
                                    sectionObject['categoryObjects'] = categoryObjectResults;
                                }

                                //Get each product id for the given category and add it to an array of product objects
                                var productObjectResults = [];
                                var count = 0;
                                _.each(categoryObject.Products, function(productId) {
                                    var daoProduct = new pb.DAO();
                                    var placeHolderProduct = {};
                                    productObjectResults.push(placeHolderProduct);
                                    daoProduct.loadById(productId, 'custom_object', {}, function(err, productObject) {
                                        count++;
                                        if(util.isError(err)) {
                                            return self.reqHandler.serveError(err);
                                        }

                                        _.extendOwn(placeHolderProduct, productObject);

                                        //Add the products to the current category Object
                                        categoryObject['productObjects'] = productObjectResults;

                                        if(count == categoryObject.Products.length) {
                                            categoryObjectResults.push(categoryObject);

                                            //Add the categories to the current section Object
                                            sectionObject['categoryObjects'] = categoryObjectResults;

                                            //Register the angular array of section objects
                                            var angularObjects = pb.ClientJs.getAngularObjects({ sectionObjects: sectionObjects });
                                            self.ts.registerLocal('section_objects', new pb.TemplateValue(angularObjects, false));
                                        }
                                    });
                                });
                            }
                        });
                    });
                });

                //Register the angular array of section objects
                var angularObjects = pb.ClientJs.getAngularObjects({ sectionObjects: sectionObjects });
                self.ts.registerLocal('section_objects', new pb.TemplateValue(angularObjects, false));

                self.ts.registerLocal('page_name', new pb.TemplateValue('Prices | Demo Website', false)); //TODO: Stop using hard coded values

                self.ts.load('prices', function(err, result) {
                    if (util.isError(err)) {
                        return cb(err);
                    }
                    cb({content: result});
                });
            });
        });
    };

    PriceObjects.prototype.stringToHex = function (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;

        for (; i < tmp_len; i += 1) {
            c = tmp.charCodeAt(i);
            str += this.d2h(c);
        }
        return str;
    };
    PriceObjects.prototype.d2h = function(number) {
        if (number < 0) {
            number = 0xFFFFFFFF + number + 1;
        }
        return this.pad(number.toString(16), 2);
    };
    PriceObjects.prototype.pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    /**
     * Provides the routes that are to be handled by an instance of this prototype.
     * The route provides a definition of path, permissions, authentication, and
     * expected content type.
     * Method is optional
     * Path is required
     * Permissions are optional
     * Access levels are optional
     * Content type is optional
     *
     * @param cb A callback of the form: cb(error, array of objects)
     */
    PriceObjects.getRoutes = function(cb) {
        var routes = [
            {
                method: 'get',
                path: '/page/product/prices',
                auth_required: false,
                content_type: 'text/html'
            }
        ];
        cb(null, routes);
    };

    return PriceObjects;
};