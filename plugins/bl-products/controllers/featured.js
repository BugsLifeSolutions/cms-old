/*
 Copyright (C) 2015  PencilBlue, LLC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//dependencies
var path  = require('path');
var async = require('async');
var _     = require('underscore');

module.exports = function featuredObject(pb) {

    //pb dependencies
    var util           = pb.util;
    var config         = pb.config;
    var TopMenu        = pb.TopMenuService;
    var Comments       = pb.CommentService;
    var ArticleService = pb.ArticleService;

    /**
     * Index page of the portfolio plugin
     * @deprecated Since 0.4.1
     * @class Index
     * @constructor
     * @extends BaseController
     */
    function featuredObject(){}
    util.inherits(featuredObject, pb.BaseController);

    featuredObject.prototype.render = function(cb) {
        var self = this;

        var contentService = new pb.ContentService();
        contentService.getSettings(function(err, contentSettings) {
            self.gatherData(function(err, data) {

                var articleService = new pb.ArticleService();
                articleService.getMetaInfo(data.content[0], function(err, meta) {
                    self.ts.registerLocal('navigation', new pb.TemplateValue(data.nav.navigation, false));
                });
            });
        });

        // Custom Product Category
        var daoPage = new pb.DAO();
        daoPage.q('page', {where: {url: 'home'}, limit: 1}, function(err, pageObject) {
            if (util.isError(err)) {
                return self.serveError(err);
            }
            else if (!util.isObject(pageObject[0])) {
                return self.reqHandler.serve404();
            }

            var daoProducts = new pb.DAO();
            var productOpts = {
                where: {
                    Featured: true,
                    Online: true
                },
                limit: 10
            };
            daoProducts.q('custom_object', productOpts, function(err, productObjects) {
                if (util.isError(productObjects)) {
                    return self.reqHandler.serveError(err);
                }

                _.each(productObjects, function(productObject) {
                    var imageUrl = '/public/bl-demo/img/placeholders/placeholder.png';
                    var imageName = 'Placeholder';
                    var count = 0;
                    _.each(productObject.Media, function(mediaId) {
                        count++;
                        var daoProductMedia = new pb.DAO();
                        (function(count_copy) {
                            daoProductMedia.loadById(mediaId, 'media', {}, function(err, mediaObject) {
                                if(util.isError(mediaObject)) {
                                    return self.reqHandler.serveError(err);
                                }
                                if(mediaObject !== null && mediaObject.media_type == 'image') {
                                    imageUrl = mediaObject.location;
                                    imageName = mediaObject.name;
                                }

                                productObject['imageUrl'] = imageUrl;
                                productObject['imageName'] = imageName;

                                if(count_copy == productObject.Media.length) {
                                    //Register the angular array of product objects
                                    var angularObjects = pb.ClientJs.getAngularObjects({ productObjects: productObjects });
                                    self.ts.registerLocal('product_objects', new pb.TemplateValue(angularObjects, false));
                                }
                            });
                        })(count);
                    });
                    productObject['imageUrl'] = imageUrl;
                    productObject['imageName'] = imageName;
                });

                //Register the angular array of product objects
                var angularObjects = pb.ClientJs.getAngularObjects({ productObjects: productObjects });
                self.ts.registerLocal('product_objects', new pb.TemplateValue(angularObjects, false));

                //Load the content for the page
                self.ts.registerLocal('page_name', new pb.TemplateValue(pageObject[0].headline + ' | Demo Website', false)); //TODO: Stop using hard coded values
                self.ts.registerLocal('page_headline', new pb.TemplateValue(pageObject[0].headline, false));
                self.ts.registerLocal('page_content', new pb.TemplateValue(pageObject[0].page_layout, false));

                self.ts.load('featured', function(err, result) {
                    if (util.isError(err)) {
                        return cb(err);
                    }
                    cb({content: result});
                });
            });
        });
    };

    featuredObject.prototype.gatherData = function(cb) {
        var self  = this;
        var tasks = {

            //navigation
            nav: function(callback) {
                self.getNavigation(function(themeSettings, navigation, accountButtons) {
                    callback(null, {themeSettings: themeSettings, navigation: navigation, accountButtons: accountButtons});
                });
            },

            //articles, pages, etc.
            content: function(callback) {
                self.loadContent(callback);
            },

            section: function(callback) {
                if(!self.req.pencilblue_section) {
                    callback(null, {});
                    return;
                }

                var dao = new pb.DAO();
                dao.loadById(self.req.pencilblue_section, {}, 'section', callback);
            }
        };
        async.parallel(tasks, cb);
    };

    featuredObject.prototype.loadContent = function(articleCallback) {

        var section = this.req.pencilblue_section || null;
        var topic   = this.req.pencilblue_topic   || null;
        var article = this.req.pencilblue_article || null;
        var page    = this.req.pencilblue_page    || null;

        //get service context
        var opts = this.getServiceContext();

        var service = new ArticleService();
        if(this.req.pencilblue_preview) {
            if(this.req.pencilblue_preview == page || article) {
                if(page) {
                    service.setContentType('page');
                }
                var where = pb.DAO.getIdWhere(page || article);
                where.draft = {$exists: true};
                where.publish_date = {$exists: true};
                service.find(where, opts, articleCallback);
            }
            else {
                service.find({}, opts, articleCallback);
            }
        }
        else if(section) {
            service.findBySection(section, articleCallback);
        }
        else if(topic) {
            service.findByTopic(topic, articleCallback);
        }
        else if(article) {
            service.findById(article, articleCallback);
        }
        else if(page) {
            service.setContentType('page');
            service.findById(page, articleCallback);
        }
        else{
            service.find({}, opts, articleCallback);
        }
    };

    featuredObject.prototype.getNavigation = function(cb) {
        var options = {
            currUrl: this.req.url,
            session: this.session,
            ls: this.ls,
            activeTheme: this.activeTheme
        };

        var menuService = new pb.TopMenuService();
        menuService.getNavItems(options, function(err, navItems) {
            if (util.isError(err)) {
                pb.log.error('featuredObject[0]: %s', err.stack);
            }
            cb(navItems.themeSettings, navItems.navigation, navItems.accountButtons);
        });
    };

    /**
     * Provides the routes that are to be handled by an instance of this prototype.
     * The route provides a definition of path, permissions, authentication, and
     * expected content type.
     * Method is optional
     * Path is required
     * Permissions are optional
     * Access levels are optional
     * Content type is optional
     *
     * @param cb A callback of the form: cb(error, array of objects)
     */
    featuredObject.getRoutes = function(cb) {
        var routes = [
            {
                method: 'get',
                path: '/page/home',
                auth_required: false,
                content_type: 'text/html'
            }
        ];
        cb(null, routes);
    };

    //exports
    return featuredObject;
};