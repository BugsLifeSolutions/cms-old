/**
 * Created by HadenHiles on 15-09-11.
 */

module.exports = function ProductsModule(pb) {

    /**
     * Products - A Products plugin for PencilBlue
     *
     * @author Blake Callens <blake@pencilblue.org>
     * @copyright 2014 PencilBlue, LLC
     */
    function Products(){}

    /**
     * Called when the application is being installed for the first time.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Products.onInstall = function(cb) {
        var self = this;

        var cos = new pb.CustomObjectService();

        cos.loadTypeByName('Products', function(err, productType) {
            if (!productType) {
                //Setup values
                var productValues = {
                    name: 'Products',
                    fields: {
                        name: {
                            field_type: 'text'
                        },
                        "Display Name": {
                            field_type: 'text'
                        },
                        Price: {
                            field_type: 'number'
                        },
                        Online: {
                            field_type: 'boolean'
                        },
                        Featured: {
                            field_type: 'boolean'
                        },
                        Content: {
                            field_type: 'wysiwyg'
                        },
                        Media: {
                            field_type: 'child_objects',
                            object_type: 'media'
                        }
                    }
                };
                cos.saveType(productValues, function (err, productType){
                    cb(err, true);
                });

                cos.loadTypeByName('Product Categories', function (err, categoryType) {
                    if (!categoryType) {
                        var categoryValues = {
                            name: 'Product Categories',
                            fields: {
                                name: {
                                    field_type: 'text'
                                },
                                "Display Name": {
                                    field_type: 'text'
                                },
                                Content: {
                                    field_type: 'wysiwyg'
                                },
                                Media: {
                                    field_type: 'child_objects',
                                    object_type: 'media'
                                },
                                Products: {
                                    field_type: 'child_objects',
                                    object_type: 'custom:Products'
                                }
                            }
                        };
                        cos.saveType(categoryValues, function (err, categoryType) {
                            cb(err, true);
                        });

                        cos.loadTypeByName('Product Sections', function (err, sectionType) {
                            if (!sectionType) {
                                var sectionValues = {
                                    name: 'Product Sections',
                                    fields: {
                                        name: {
                                            field_type: 'text'
                                        },
                                        "Display Name": {
                                            field_type: 'text'
                                        },
                                        Content: {
                                            field_type: 'wysiwyg'
                                        },
                                        Media: {
                                            field_type: 'child_objects',
                                            object_type: 'media'
                                        },
                                        "Categories": {
                                            field_type: 'child_objects',
                                            object_type: 'custom:Product Categories'
                                        }
                                    }
                                };
                                cos.saveType(sectionValues, function (err, sectionType) {
                                    cb(err, true);
                                });
                            } else {
                                cb(null, true);
                            }
                        });
                    } else {
                        cb(null, true);
                    }
                });
            } else {
                cb(null, true);
            }
        });
    };

    /**
     * Called when the application is uninstalling this plugin.  The plugin should
     * make every effort to clean up any plugin-specific DB items or any in function
     * overrides it makes.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Products.onUninstall = function(cb) {
        cb(null, true);
    };

    /**
     * Called when the application is starting up. The function is also called at
     * the end of a successful install. It is guaranteed that all core PB services
     * will be available including access to the core DB.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Products.onStartup = function(cb) {
        cb(null, true);
    };

    /**
     * Called when the application is gracefully shutting down.  No guarantees are
     * provided for how much time will be provided the plugin to shut down.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Products.onShutdown = function(cb) {
        cb(null, true);
    };

    //exports
    return Products;
};
