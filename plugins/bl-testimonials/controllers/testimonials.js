/*
 Copyright (C) 2015  PencilBlue, LLC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//dependencies
var path  = require('path');
var async = require('async');
var _     = require('underscore');

module.exports = function TestimonialObjects(pb) {

    //pb dependencies
    var util           = pb.util;
    var config         = pb.config;
    var TopMenu        = pb.TopMenuService;
    var Comments       = pb.CommentService;
    var ArticleService = pb.ArticleService;

    /**
     * Index page of the Testimonial plugin
     * @deprecated Since 0.4.1
     * @class Index
     * @constructor
     * @extends BaseController
     */
    function TestimonialObjects(){}
    util.inherits(TestimonialObjects, pb.BaseController);

    TestimonialObjects.prototype.render = function(cb) {
        var self = this;

        var contentService = new pb.ContentService();
        contentService.getSettings(function(err, contentSettings) {
            self.gatherData(function(err, data) {

                var articleService = new pb.ArticleService();
                articleService.getMetaInfo(data.content[0], function(err, meta) {
                    self.ts.registerLocal('navigation', new pb.TemplateValue(data.nav.navigation, false));
                });
            });
        });

        //Load the content for the page
        var daoContent = new pb.DAO();
        daoContent.q('page', {where: {url: 'testimonials'}, limit: 1}, function(err, pageObject) {
            if (util.isError(err)) {
                return self.serveError(err);
            }
            else if (!util.isObject(pageObject)) {
                return self.reqHandler.serve404();
            }
            self.ts.registerLocal('page_headline', new pb.TemplateValue(pageObject[0].headline, false));
            self.ts.registerLocal('page_content', new pb.TemplateValue(pageObject[0].page_layout, false));
        });

        // Custom Testimonial Code
        var opts = {
            where: {name: 'Testimonials'},
            limit: 1
        };
        var dao = new pb.DAO();
        dao.q('custom_object_type', opts, function(err, testimonialObjectType) {
            if (util.isError(err)) {
                return self.serveError(err);
            }
            else if (!util.isObject(testimonialObjectType)) {
                return self.reqHandler.serve404();
            }
            var testimonialTypeObjectId = self.stringToHex(testimonialObjectType[0]._id.id);

            var opts1 = {
                where: {type: testimonialTypeObjectId},
                limit: 100,
                order: {"Testimony Date": -1}
            };
            var dao1 = new pb.DAO();
            dao1.q('custom_object', opts1, function(err, testimonialObjects) {
                if (util.isError(testimonialObjects)) {
                    return self.reqHandler.serveError(err);
                }

                var daoProductMedia = new pb.DAO();
                _.each(testimonialObjects, function(testimonialObject) {
                    var imageUrl = '/public/bl-elite-party-rentals/img/placeholders/person.jpg';
                    var imageName = 'Placeholder Person';
                    daoProductMedia.loadById(testimonialObject.Photo, 'media', {}, function(err, mediaObject) {
                        if(util.isError(mediaObject)) {
                            return self.reqHandler.serveError(err);
                        }
                        if(mediaObject !== null && mediaObject.media_type == 'image') {
                            imageUrl = mediaObject.location;
                            imageName = mediaObject.name;
                        }

                        testimonialObject['imageUrl'] = imageUrl;
                        testimonialObject['imageName'] = imageName;

                        //Register the angular array of objects
                        var angularObjects = pb.ClientJs.getAngularObjects({
                            customObjects: testimonialObjects
                        });
                        self.ts.registerLocal('angular_objects', new pb.TemplateValue(angularObjects, false));
                    });
                });

                //Register the angular array of objects
                var angularObjects = pb.ClientJs.getAngularObjects({
                    customObjects: testimonialObjects
                });
                self.ts.registerLocal('angular_objects', new pb.TemplateValue(angularObjects, false));

                self.ts.registerLocal('page_name', new pb.TemplateValue('Testimonials | Demo Website', false));

                self.ts.load('testimonials', function(err, result) {
                    cb({content: result});
                });
            });
        });
    };

    TestimonialObjects.prototype.gatherData = function(cb) {
        var self  = this;
        var tasks = {

            //navigation
            nav: function(callback) {
                self.getNavigation(function(themeSettings, navigation, accountButtons) {
                    callback(null, {themeSettings: themeSettings, navigation: navigation, accountButtons: accountButtons});
                });
            },

            //articles, pages, etc.
            content: function(callback) {
                self.loadContent(callback);
            },

            section: function(callback) {
                if(!self.req.pencilblue_section) {
                    callback(null, {});
                    return;
                }

                var dao = new pb.DAO();
                dao.loadById(self.req.pencilblue_section, {}, 'section', callback);
            }
        };
        async.parallel(tasks, cb);
    };

    TestimonialObjects.prototype.loadContent = function(articleCallback) {

        var section = this.req.pencilblue_section || null;
        var topic   = this.req.pencilblue_topic   || null;
        var article = this.req.pencilblue_article || null;
        var page    = this.req.pencilblue_page    || null;

        //get service context
        var opts = this.getServiceContext();

        var service = new ArticleService();
        if(this.req.pencilblue_preview) {
            if(this.req.pencilblue_preview == page || article) {
                if(page) {
                    service.setContentType('page');
                }
                var where = pb.DAO.getIdWhere(page || article);
                where.draft = {$exists: true};
                where.publish_date = {$exists: true};
                service.find(where, opts, articleCallback);
            }
            else {
                service.find({}, opts, articleCallback);
            }
        }
        else if(section) {
            service.findBySection(section, articleCallback);
        }
        else if(topic) {
            service.findByTopic(topic, articleCallback);
        }
        else if(article) {
            service.findById(article, articleCallback);
        }
        else if(page) {
            service.setContentType('page');
            service.findById(page, articleCallback);
        }
        else{
            service.find({}, opts, articleCallback);
        }
    };

    TestimonialObjects.prototype.getNavigation = function(cb) {
        var options = {
            currUrl: this.req.url,
            session: this.session,
            ls: this.ls,
            activeTheme: this.activeTheme
        };

        var menuService = new pb.TopMenuService();
        menuService.getNavItems(options, function(err, navItems) {
            if (util.isError(err)) {
                pb.log.error('TestimonialObjects: %s', err.stack);
            }
            cb(navItems.themeSettings, navItems.navigation, navItems.accountButtons);
        });
    };

    TestimonialObjects.prototype.stringToHex = function (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;

        for (; i < tmp_len; i += 1) {
            c = tmp.charCodeAt(i);
            str += this.d2h(c);
        }
        return str;
    };
    TestimonialObjects.prototype.d2h = function(number) {
        if (number < 0) {
            number = 0xFFFFFFFF + number + 1;
        }
        return this.pad(number.toString(16), 2);
    };
    TestimonialObjects.prototype.pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    /**
     * Provides the routes that are to be handled by an instance of this prototype.
     * The route provides a definition of path, permissions, authentication, and
     * expected content type.
     * Method is optional
     * Path is required
     * Permissions are optional
     * Access levels are optional
     * Content type is optional
     *
     * @param cb A callback of the form: cb(error, array of objects)
     */
    TestimonialObjects.getRoutes = function(cb) {
        var routes = [
            {
                method: 'get',
                path: '/page/testimonials',
                auth_required: false,
                content_type: 'text/html'
            }
        ];
        cb(null, routes);
    };

    //exports
    return TestimonialObjects;
};