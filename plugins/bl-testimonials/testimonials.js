/**
 * Created by HadenHiles on 15-09-11.
 */

module.exports = function TestimonialsModule(pb) {

    /**
     * Testimonials - A Testimonials plugin for PencilBlue
     *
     * @author Blake Callens <blake@pencilblue.org>
     * @copyright 2014 PencilBlue, LLC
     */
    function Testimonials(){}

    /**
     * Called when the application is being installed for the first time.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Testimonials.onInstall = function(cb) {
        var self = this;

        var cos = new pb.CustomObjectService();
        cos.loadTypeByName('Testimonials', function(err, testimonialType) {
            if (!testimonialType) {
                var testimonialValues = {
                    name: 'Testimonials',
                    fields: {
                        name: {
                            field_type: 'text'
                        },
                        "Display Name": {
                            field_type: 'text'
                        },
                        "Testimony Date": {
                            field_type: 'date'
                        },
                        Message: {
                            field_type: 'wysiwyg'
                        },
                        Photo: {
                            field_type: 'peer_object',
                            object_type: 'media'
                        }
                    }
                };

                cos.saveType(testimonialValues, function (err, testimonialType) {
                    cb(err, true);
                });
            } else {
                cb(null, true);
            }
        });
    };

    /**
     * Called when the application is uninstalling this plugin.  The plugin should
     * make every effort to clean up any plugin-specific DB items or any in function
     * overrides it makes.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Testimonials.onUninstall = function(cb) {
        cb(null, true);
    };

    /**
     * Called when the application is starting up. The function is also called at
     * the end of a successful install. It is guaranteed that all core PB services
     * will be available including access to the core DB.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Testimonials.onStartup = function(cb) {
        cb(null, true);
    };

    /**
     * Called when the application is gracefully shutting down.  No guarantees are
     * provided for how much time will be provided the plugin to shut down.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Testimonials.onShutdown = function(cb) {
        cb(null, true);
    };

    //exports
    return Testimonials;
};
