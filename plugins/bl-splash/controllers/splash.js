/*
 Copyright (C) 2015  PencilBlue, LLC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//dependencies
var path  = require('path');
var async = require('async');

module.exports = function SplashObjects(pb) {

    //pb dependencies
    var util           = pb.util;

    /**
     * Index page of the splash plugin
     * @deprecated Since 0.4.1
     * @class Index
     * @constructor
     * @extends BaseController
     */
    function SplashObjects(){}
    util.inherits(SplashObjects, pb.BaseController);

    SplashObjects.prototype.render = function(cb) {
        var self = this;
        self.ts.load('index', function(err, result) {
            cb({content: result});
        });
    };

    /**
     * Provides the routes that are to be handled by an instance of this prototype.
     * The route provides a definition of path, permissions, authentication, and
     * expected content type.
     * Method is optional
     * Path is required
     * Permissions are optional
     * Access levels are optional
     * Content type is optional
     *
     * @param cb A callback of the form: cb(error, array of objects)
     */
    SplashObjects.getRoutes = function(cb) {
        var routes = [
            {
                method: 'get',
                path: '/',
                auth_required: false,
                content_type: 'text/html'
            }
        ];
        cb(null, routes);
    };

    //exports
    return SplashObjects;
};